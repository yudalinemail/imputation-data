import pandas as pd
import numpy as np
import xlwt

from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
from sklearn.multioutput import MultiOutputRegressor
from sklearn.preprocessing import StandardScaler

from sklearn import ensemble                        #RF: https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/ensemble/
from sklearn.neighbors import KNeighborsRegressor   #KNN: https://github.com/scikit-learn/scikit-learn/tree/master/sklearn/neighbors
from sklearn import tree                            #CART: https://github.com/scikit-learn/scikit-learn/tree/master/sklearn/tree

##############################################################
#Please select a imputation method.
# 1. knn
# 2. cart
# 3. rf

imputation_method = 'cart' 

#input dataset name
dataset_name = "your_data.xlsx"
##############################################################

#Convert to Data Frame
df = pd.read_excel(dataset_name)

feature_field = np.array(df.columns)
feature_field = feature_field.tolist()#list

#Delete samples with missing
no_nan_df = df.dropna() 
no_nan_df = np.array(no_nan_df)
no_nan_df = no_nan_df.tolist()#list

data_array = np.array(df)
data_array = data_array.tolist()

#Encoding discrete feature(location)
reg_array = []
location_name = []

for i in range(len(data_array)):
    reg_array.append(data_array[i][12])
    location_name.append(data_array[i][12])
reg = np.unique(reg_array)
reg = list(reg)

le_f = LabelEncoder()
le_f.fit(reg)
reg_array = le_f.transform(reg_array).tolist()   
reg = le_f.transform(reg).tolist()
reg = np.array(reg)
reg=reg.reshape(-1,1)

enc = OneHotEncoder(categories='auto')
enc.fit(reg)
reg_array = np.array(reg_array)
reg_array=reg_array.reshape(-1,1)
reg_array = enc.transform(reg_array).toarray()

reg_array = reg_array.tolist()

#The encoded value is added to data_array
for i in range(len(data_array)): 
    data_array[i].pop()
    data_array[i].extend(reg_array[i])
    
#Standardization
Standard_encoding = StandardScaler()  
data_array = Standard_encoding.fit_transform(data_array)
data_array = np.array(data_array)
data_array = data_array.tolist()

#Identify missing features
df_nan_array = df.isnull() 
df_nan_array = np.array(df_nan_array)
df_nan_array = df_nan_array.tolist()

#Identify missing all feature combinations
missing_combination = []
nan_index_array = [] 
nan_position_array = []
for i in range(len(df_nan_array)):
    nan_index = []
    missing_combination.append([])
    for j in range(len(df_nan_array[i])):
        if df_nan_array[i][j] == True: 
            nan_index_array.append(i)
            nan_position_array.append(j)    
            nan_index.append(j)
            missing_combination[i].append(j)
        count = len(nan_index)
        

    
#Determine the unique feature combination
nan_datarry = []
mixing_array = []
for i in range(len(data_array)):
    mixing_array.append([])
    
for i in range(len(nan_position_array)): 
    mixing_array[nan_index_array[i]].append(nan_position_array[i]) 
    nan_datarry.append(nan_position_array[i])
    
nan_unique_array = np.unique(nan_index_array)
missing_combination = np.unique(missing_combination)
missing_combination = missing_combination.tolist()#list
missing_combination.pop(0)

nan_array = [] 
no_nan_df = []
for i in range(len(data_array)):
    reg_bl = False
    for j in nan_unique_array: 
        if i == j:
            reg_bl = True
    if reg_bl == True:
        nan_array.append(data_array[i])
    else:
        no_nan_df.append(data_array[i])
        
#--------------------------feature_Split----------------------#
def feature_Split(array,target_array): 
    '''
    input:
        array : array or list(1D to 2D shape can be input)
        target_array : array or list[shape:1D]
    output:
        data_array : list [shape:1D or 2D]
        feature_array : list [shape:1D or 2D]
        
    example    
        input:
            array =[[1,2,3],
                    [4,5,6]]
            target_array = [1]
            
        output:
            data_array = [[1,3],
                          [4,6]]
            feature_array = [[2],
                             [5]]
    '''
    data_array = []
    feature_array = []
    try: 
        array[0][0]     
    except:
        data_array.append([])
        feature_array.append([])
        for l in range(len(array)):
            count = False
            for j in target_array:
                if l == j:
                   count = True
            if count == True:
                feature_array[0].append(array[l])
            else:
                data_array[0].append(array[l])
        return data_array,feature_array
    
    for l in range(len(array)):
        data_array.append([])
        feature_array.append([])
        for i in range(len(array[l])):
            count = False
            for j in target_array:
                if i == j:
                   count = True
            if count == True:
                feature_array[l].append(array[l][i])
            else:
                data_array[l].append(array[l][i])
    return data_array,feature_array
#--------------------------feature_Split----------------------#

#----------------------------Training model--------------------------#
def Modeling(x_train, y_train, imputation_method = 'knn'):
    '''
    input:
        x_train : list or array, [shape:2D]
                    feature
        y_train : list or array,  [shape:1D or 2D]
                    label
        imputation_method : str, default = 'knn' (knn, cart or rf) 
            
    output:
        clf : imputation model
    '''
    if imputation_method =='knn' or imputation_method =='KNN':
        clf = KNeighborsRegressor(n_neighbors = 3, weights = "distance")
    elif imputation_method =='cart' or imputation_method =='CART':
        clf = tree.DecisionTreeRegressor()
    elif imputation_method =='rf' or imputation_method =='RF':
        clf = ensemble.RandomForestRegressor(n_estimators=20)
    
    clf = MultiOutputRegressor(clf)
    clf.fit(x_train,y_train)
    return clf
#---------------------------Training model--------------------------#
    
#Training model for missing combinations
mode_array = []
for i in range(len(missing_combination)):
    feature,label = feature_Split(no_nan_df,missing_combination[i])
    mode_array.append(Modeling(feature, label,imputation_method))
    
#Use the trained model to imputation missing value
prediction_array = []
for i in range(len(mixing_array)):
    prediction_array.append([])
    if mixing_array[i] != []: 
        for j in range(len(missing_combination)):
            if mixing_array[i] == missing_combination[j]:
                feature,label = feature_Split(data_array[i],missing_combination[j]) 
                prediction_array[i].extend(mode_array[j].predict(feature))

for i in range(len(mixing_array)):
    if mixing_array[i] != []: #Determine if there are missing values
        prediction_array[i] = prediction_array[i][0].tolist()
        for j in range(len(mixing_array[i])): #Fill data according to missing positions
            data_array[i][mixing_array[i][j]] = prediction_array[i][j]
            
#-------------------------------Reverse processing----------------------------------#
data_array = Standard_encoding.inverse_transform(data_array)
#rounding
for i in range(len(data_array)):
    for j in range(len(data_array[i])):
        data_array[i][2] = round(data_array[i][2])
        data_array[i][3] = round(data_array[i][3])
        data_array[i][4] = round(data_array[i][4])
        data_array[i][5] = round(data_array[i][5],1)
        data_array[i][6] = round(data_array[i][6])  
        data_array[i][7] = round(data_array[i][7],1)
        data_array[i][8] = round(data_array[i][8],1)
        data_array[i][11] = round(data_array[i][11],1)
        
test_array = data_array.tolist()
data_array = data_array.tolist()

#feature(location) of replacement one-hot encoding
for i in range(len(data_array)):
    for j in range(len(reg_array[i])): 
        data_array[i].pop() 
    data_array[i].append(location_name[i])
    
data_array.insert(0, feature_field)
mixing_array.insert(0, [])
#-----------------------------Reverse processing END---------------------------------#
    
#Output the imputation datasets
def setStyle(name, height,color, bold=False):
    style = xlwt.XFStyle()  
    font = xlwt.Font() 
    font.name = name
    font.colour_index = color
    font.height = height
    style.font = font
    return style

f= xlwt.Workbook(encoding='utf8', style_compression=2)
sheet1 = f.add_sheet(u'colour', cell_overwrite_ok=False)
column = 0;
row = 0

for i in range(len(data_array)):
    for j in range(len(data_array[i])):
        bl_0 = False
        if mixing_array[i] != []: 
            for k in range(len(mixing_array[i])):
                if mixing_array[i][k] == j:
                    bl_0 = True
        '''
        Original value : black
        Imputation value: red
        '''
        if bl_0 == True:
            sheet1.write(i, j,data_array[i][j], setStyle('Times New Roman', 240, 2, False)) 
        else:
            sheet1.write(i, j,data_array[i][j], setStyle('Times New Roman', 240, 0, False))

f.save(r'Imputation_datasets_' + imputation_method +'.xls')   
